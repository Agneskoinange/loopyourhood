FROM python:3.8


ENV PYTHONUNBUFFERED 1
ENV DJANGO_SETTINGS_MODULE project.settings


WORKDIR /loopyourhood

COPY . /loopyourhood/

RUN pip install -r requirements.txt


EXPOSE 8000


CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
